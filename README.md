# Map Note Hover Display

A single-purpose module that provides a HUD view of a map note's journal entry contents when hovered.

It provides some basic customization options in the settings, such as a light theme and a dark theme.

# Usage

Just install the package and then opt in for yourself in the settings. This package requires each user to opt in if they want to see the hover displays so that it's not annoying for those who don't need it.

# Screenshot

![Screenshot](https://gitlab.com/friendlylambda/map-note-hover-display/-/raw/b8cae1bf64019c9fd6068393dc5d42e42fb2d274/screenshot.png)

# Credit

I developed this module based on the functionality in [Pin Cushion](https://github.com/death-save/pin-cushion)—thank you to [Death Save Development](https://github.com/death-save) for their great work!
